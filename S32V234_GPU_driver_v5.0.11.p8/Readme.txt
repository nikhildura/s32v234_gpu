GPU driver version 5.0.11.p8 release for S32V234
This version requires at least Linux BSP6.1

Folder contents:
	- Galcore_kernel_module:
		SW item: Driver kernel space
		Description: kernel driver part
		License per SCR: Dual license GPL and MIT
		Content: Archive with sources
	- Vivante_userspace_libraries_and_demos:
		SW item: Driver user space
		Description: Dynamic libraries required for running GPU applications, as well as some precompiled demos
		License per SCR: LA_OPT_BASE_LICENSE
		Content: Linux installer with click-through license. 
	- Vivate_VTK:
		SW item: Vivante Tool Kit (VTK) 
		Description: A series a applications that help in debugging your GPU code
		License per SCR: LA_OPT_BASE_LICENSE
		Content: An User Guide and a number of tool installers and configuration files.